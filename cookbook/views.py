from django.shortcuts import render
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Recipe, Ingredient, Tag
from .serializers import RecipeSerializer, IngredientSerializer, TagSerializer
from django.core.exceptions import ObjectDoesNotExist

class RecipeView(APIView):
    def post(self, request):
        serializer = RecipeSerializer(data=request.data)
        
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        recipe = Recipe.objects.create(
            name=request.data['name'],
            description=request.data['description'],
            instructions=request.data['instructions']
        )

        for ingredient in request.data['ingredient_set']:
            Ingredient.objects.create(**ingredient, recipe=recipe)

        for tag in request.data['tags']:
            newTag = Tag.objects.create(**tag, recipe=recipe)
            recipe.tags.add(newTag)
        
        serializer = RecipeSerializer(recipe)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def get(self, request, recipe_id=''):
        if recipe_id == '':
            queryset = Recipe.objects.all()
            serializer = RecipeSerializer(queryset, many=True)
            return Response(serializer.data)
        
        try:
            recipe = Recipe.objects.get(id=recipe_id)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = RecipeSerializer(recipe)
        return Response(serializer.data)

    def delete(self, request, recipe_id):
        try:
            recipe = Recipe.objects.get(id=recipe_id)
        except ObjectDoesNotExist:
            return Response(serializer.data, status=status.HTTP_404_NOT_FOUND)
        
        recipe.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

