from rest_framework import serializers

class IngredientSerializer(serializers.Serializer):
    # id = serializers.IntegerField(read_only=True)
    name = serializers.CharField()
    unit = serializers.CharField()
    amount = serializers.IntegerField()

class TagSerializer(serializers.Serializer):
    # id = serializers.IntegerField(read_only=True)
    name = serializers.CharField()

class RecipeSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField()
    description = serializers.CharField()
    instructions = serializers.CharField()
    ingredient_set = IngredientSerializer(many=True)
    tags = TagSerializer(many=True)
