from django.db import models

class Tag(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.name}'

class Recipe(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    instructions = models.TextField()
    tags = models.ManyToManyField(Tag)

    def __str__(self):
        return f'{self.id} - {self.name} - {self.description} - {self.instructions}'

class Ingredient(models.Model):
    name = models.CharField(max_length=255)
    unit = models.CharField(max_length=3)
    amount = models.IntegerField()
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.name} - {self.unit} - {self.amount}'

