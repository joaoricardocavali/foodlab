Primeiro foi criado as models do Recipe, Ingredients e Tag e depois as Views respectivas.
Depois foram criados os serializers e importadas nas views para montagem das lógicas.
Por fim, foram feitos testes no insomnia para ver se todas as rotas e lógicas estavam retornando o esperado.